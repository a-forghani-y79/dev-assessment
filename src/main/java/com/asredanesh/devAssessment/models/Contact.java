package com.asredanesh.devAssessment.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String phoneNumber;

    private String email;

    private String organization;

    private String github;

    @ElementCollection
    private List<String> repos ;

    public Contact() {
    }
}
