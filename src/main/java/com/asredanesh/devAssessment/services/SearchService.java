package com.asredanesh.devAssessment.services;

import com.asredanesh.devAssessment.models.Contact;
import com.asredanesh.devAssessment.repositories.ContactsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchService {

    @Autowired
    private ContactsRepo repo;

    public List<Contact> search(Contact contact) {

        ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreCase().withIgnoreNullValues().withIgnorePaths("id");
        Example<Contact> example = Example.of(contact,matcher);
        System.out.println("requested");
        System.out.println(contact.toString());
        List<Contact> list = new ArrayList<>(repo.findAll(example));
        System.out.println("print data");
        list.forEach(System.out::println);
        return list;
    }

     public List<Contact> findAll() {
        return repo.findAll();
    }


}
