package com.asredanesh.devAssessment.services;

import com.asredanesh.devAssessment.services.exceptions.AccountNotFoundException;
import com.google.gson.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class Github {

    @Autowired
    private HTTPClient client = new HTTPClient();

    private String address;
    private HttpResponse<String> responseEntity;
    private JsonElement jsonElement;

    public void setAddress(String address) {
        this.address = address;
    }

    //send GET to api.github.com with HTTPClient class and init JsonObject
    public void init() throws IOException, InterruptedException, AccountNotFoundException {
        client.setUsername(extractUsername());
        client.get();
        responseEntity = client.getResponse();
        if (responseEntity.statusCode()==404)
            throw new AccountNotFoundException();
        this.jsonElement = JsonParser.parseString(responseEntity.body());
    }

    //extract username from this.address
    private String extractUsername() {
        String username = address.toLowerCase();
        if (username.contains("github.com"))
            username = username.substring(username.lastIndexOf('/') + 1);
        if (username.contains("@"))
            username = username.substring(username.lastIndexOf("@") + 1);
        return username;
    }


    public String getUsername() {
        return extractUsername();
    }

    public List<String> getReposList() {
        List<String> data = new ArrayList<>();
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jo = (JsonObject) jsonArray.get(i);
            String fullName = jo.get("name").toString();
            fullName = fullName.substring(1, fullName.length() - 1);
            data.add(fullName);
        }
        return data;
    }
}
