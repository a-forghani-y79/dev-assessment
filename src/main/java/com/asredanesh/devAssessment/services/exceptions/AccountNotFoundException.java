package com.asredanesh.devAssessment.services.exceptions;

public class AccountNotFoundException extends Exception {
    public AccountNotFoundException() {
        super("Account Not Found");
    }
}
