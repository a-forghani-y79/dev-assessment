package com.asredanesh.devAssessment.services;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class HTTPClient {
    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    private String username;
    private HttpResponse<String> response;

    public void get() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://api.github.com/users/" + username+"/repos"))
                .header("Accept","application/json")
                .build();

        this.response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public HttpResponse<String> getResponse() {
        return response;
    }
}
