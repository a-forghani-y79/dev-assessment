package com.asredanesh.devAssessment.services;

import com.asredanesh.devAssessment.models.Contact;
import com.asredanesh.devAssessment.repositories.ContactsRepo;
import com.asredanesh.devAssessment.services.exceptions.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NewContactService {
    @Autowired
    private Github github;
    @Autowired
    private ContactsRepo repo;

    //get github username and use GithubService and get number of repositories. then save into h2 DB
    public void newContact(Contact contact) {
        //init github account details
        String githubUsername = contact.getGithub();
        List<String> repos = new ArrayList<>();
        github.setAddress(githubUsername);
        try {
            github.init();
            githubUsername=github.getUsername();
            repos=github.getReposList();
           
        } catch (AccountNotFoundException e){
            //not valid account
            System.out.println(e.getMessage());
        }catch(Exception e){
        //other exceptions like internet connection or etc 
    }
        contact.setGithub(githubUsername);
        contact.setRepos(repos);
        //saving
        repo.save(contact);
    }


}
