package com.asredanesh.devAssessment.endpoints;

import com.asredanesh.devAssessment.models.Contact;
import com.asredanesh.devAssessment.services.NewContactService;
import com.asredanesh.devAssessment.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.ClientInfoStatus;
import java.util.List;

@RestController
@RequestMapping(value = "/contacts")
public class Endpoints {

    @Autowired
    private NewContactService newContactService;

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void newContact(@RequestBody() Contact contact) {
        newContactService.newContact(contact);
    }


    @RequestMapping(value = "/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Contact>> search(@RequestBody() Contact contact) {
        ResponseEntity<List<Contact>> responseEntity;
        List<Contact> founded = searchService.search(contact);
        if (founded.size() > 0)
            responseEntity = ResponseEntity.ok(founded);
        else
            responseEntity = ResponseEntity.notFound().build();

        return responseEntity;

    }

}
