package com.asredanesh.devAssessment.repositories;

import com.asredanesh.devAssessment.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ContactsRepo extends JpaRepository<Contact,Integer> {

}
